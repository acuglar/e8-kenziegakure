from ninja_model import Ninja


class Jounin(Ninja):

    ninja_level = 'Jounin'

    def __init__(
        self,
        name,
        clan, 
        village,
        proficiency
        ):
        super().__init__(name, clan, village)
        self.proficiency = proficiency
        self.is_in_mission = False

    def list_best_proficiency(self):
        max_proficiency = max(self.proficiency.values())
        for k, v in self.proficiency.items():
            if v == max_proficiency:
                return f'{self.name} demonstra maior proficiência em {k}'

    def start_mission(self):
        if not self.is_in_mission:
            self.is_in_mission = True
            return f'O ninja {self.name} {self.clan} saiu em missão'
        else:
            return f'O ninja {self.name} {self.clan} já está em uma missão' 

    def return_from_mission(self):
        if self.is_in_mission:
            self.is_in_mission = False
            return f'O ninja {self.name} {self.clan} retornou em segurança da missão'
        else:
            return f'O ninja {self.name} {self.clan} não está em nenhuma missão no momento'
    
kakashi_proficiency = {'taijutsu': 7, 'ninjutsu': 8, 'genjutsu': 4}

# Criação de uma instância da classe Jounin
kakashi = Jounin('Kakashi', 'Hatake', 'Konoha', kakashi_proficiency)

print(kakashi.__dict__)
l = kakashi.list_best_proficiency()
print(l)

kakashi.start_mission()
s = kakashi.is_in_mission
print(s)

kakashi.return_from_mission()
s = kakashi.is_in_mission
print(s)