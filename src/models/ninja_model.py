from jutsu_model import Jutsu


class Ninja:
    def __init__(self, name, clan, village, *ninja_level):
        self.name = str(name)
        self.clan = str(clan)
        self.village = str(village)
        self.ninja_level = str(ninja_level) if ninja_level else "Unranked"
        self.jutsu_list = list()
        self.health_pool = 100
        self.chakra_pool = 100
        self.concious = True

    def learn_jutsu(self, jutsu):
        self.jutsu_list.append(jutsu.__dict__)
        return f'O ninja {self.name} acabou de aprender um novo jutsu: {jutsu.jutsu_name}'

    def cast_jutsu(self, jutsu, adversary) :
        if not adversary.concious:
            return False
        else: 
            if jutsu in self.jutsu_list and self.chakra_pool >= self.jutsu.chakra_spend:
                adversary.health_pool -= self.jutsu.jutsu_damage
                self.chakra_pool -= self.jutsu.chakra_spend

    @staticmethod
    def check_health(ninja_to_check): 
        if ninja_to_check.health_pool < 0:
            ninja_to_check.update({'health_pool': 0})
            ninja_to_check.update({'concious': False})
            return ninja_to_check.concious


# Criação de uma instância da classe Jutsu
rasengan = Jutsu('Rasengan', 'Vento', 'a', 20, -15)

# Criação de uma instância da classe Ninja
naruto = Ninja('Naruto', 'Uzumaki', 'Konoha')

#Chamada do método learn_jutsu
naruto.learn_jutsu(rasengan)

# Criação de uma outra instancia da classe Ninja
sasuke = Ninja('Sasuke', 'Uchiha', 'Konoha')

#Chamada do método cast_jutsu
res = naruto.cast_jutsu(rasengan, sasuke)
print(res)
