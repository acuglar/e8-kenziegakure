class Jutsu:

    jutsu_ranks = ('D', 'C', 'B', 'A', 'S',)

    def __init__(
        self,
        jutsu_name: str,
        jutsu_type: str,
        jutsu_level: str,
        jutsu_damage: int,
        chakra_spend: int,
    ):
    
        self.jutsu_name = str(jutsu_name)
        self.jutsu_type = str(jutsu_type)
        self.jutsu_level = (
            str(jutsu_level).upper()
            if jutsu_level.upper() in self.jutsu_ranks
            else "Unranked"
        )
        self.jutsu_damage = int(jutsu_damage)
        self.chakra_spend = 100 if int(chakra_spend) <= 0 else int(chakra_spend)

